var profileTemplate = $('#profile-template').html();
var renderProfile = Handlebars.compile(profileTemplate);
var reposTemplate = $('#repos-template').html();
var renderRepos = Handlebars.compile(reposTemplate);

function fetchProfile(username) {
  return $.getJSON('https://api.github.com/users/' + username);
}

function fetchRepos(username) {
  return $.getJSON('https://api.github.com/users/' + username + '/repos');
}

fetchProfile('wycats').then(function(profile) {
  var html = renderProfile(profile);
  $('#profile').html(html);
});

fetchRepos('wycats').then(function(repositories) {
  var html = renderRepos({
    repos: repositories
  });

  $('#repos').html(html);
});
